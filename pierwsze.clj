(require '[clojure.string :as str])
(defn prime? [number] 
  (let [numbers (range 2 (int (inc (Math/sqrt (inc number)))))
        rests (map (fn [x] (mod number x)) numbers)]
  (if (or (> (count(filter (fn [x] (== x 0)) rests)) 0) (== number 1))  "NIE"  "TAK"))) 

(def total-count (read-string (read-line)))
(dotimes [x total-count]
  (println (prime? (read-string (read-line)))))
