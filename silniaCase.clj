(defn silniaCase [number]
    (case number
          0 "0 1"
          1 "0 1"
          2 "0 2"
          3 "0 6"
          4 "2 4"
          5 "2 0"
          6 "2 0"
          7 "4 0"
          8 "2 0"
          9 "8 0"
          "0 0"))

(def total-count (read-string (read-line)))
(dotimes [x total-count]
    (println (silniaCase (read-string (read-line)))))

