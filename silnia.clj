(require '[clojure.string :as str])


(defn silniaCase [number]
  (case number
    0 "0 0"
    1 "0 1"
    2 "0 2"
    3 "0 6"
    4 "2 4"
    5 "2 0"
    6 "2 0"
    7 "4 0"
    8 "2 0"
    9 "8 0" 
    "0 0"))

(defn silnia [number]
  (loop [current number result 1]
  (if (= current 1)
    result
    (recur (dec current) (* result current)))))

(defn resztaSilnia [wynik]
  (let [wynikModuloPodzielony (str/split (str (rem wynik 100)) #"")]
    (do 
      (if (== (count wynikModuloPodzielony) 1) (str/join " " (reverse (conj wynikModuloPodzielony "0")))
      (str/join " " wynikModuloPodzielony)))))

(defn silniaCzyGotowy [number]
    (if (> number 10) "0 0" (resztaSilnia (silnia number)))) 

(def total-count (read-string (read-line)))
(dotimes [x total-count]
  (println (silniaCase (read-string (read-line)))))
